consensuscore (1.1.1+dfsg-7) unstable; urgency=medium

  * Team upload.
  * d/control: resume building on i386.
  * d/rules: add openmp-simd and -O3 flags.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 24 Mar 2024 17:07:25 +0100

consensuscore (1.1.1+dfsg-6) unstable; urgency=medium

  * Team upload.
  * d/patches/portable_simd: remove baseline violation and enable
    compilation on other 64-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 23 Mar 2024 19:42:10 +0100

consensuscore (1.1.1+dfsg-5) unstable; urgency=medium

  * Team upload.
  * d/rules: Don't install the Meson build file into /usr/include.
  * d/clean: remove some leftovers
  * Standards-Version: 4.6.2 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 23 Mar 2024 13:19:35 +0100

consensuscore (1.1.1+dfsg-4) unstable; urgency=medium

  * d/watch: Repository is deprecated upstream
  * Standards-Version: 4.6.1 (routine-update)
  * Fix sequence of setuptools - distutils calls
    Closes: #1024038

 -- Andreas Tille <tille@debian.org>  Mon, 14 Nov 2022 14:14:59 +0100

consensuscore (1.1.1+dfsg-3) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Set module name for autopkgtest

 -- Andreas Tille <tille@debian.org>  Fri, 15 Oct 2021 09:59:08 +0200

consensuscore (1.1.1+dfsg-2) unstable; urgency=medium

  * Use 2to3 to convert from Python2 and Python3
    Closes: #936328
  * Add myself to Uploaders
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Mon, 09 Dec 2019 14:01:32 +0100

consensuscore (1.1.1+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Exclude precompiled binaries
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Tue, 09 Oct 2018 17:43:54 +0200

consensuscore (1.0.2-3) unstable; urgency=medium

  * Team upload.
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.5
  * Testsuite: autopkgtest-pkg-python
  * d/rules:
     - hardening=+all
     - respect DEB_BUILD_OPTIONS in override_dh_auto_test

 -- Andreas Tille <tille@debian.org>  Sat, 14 Jul 2018 17:26:27 +0200

consensuscore (1.0.2-2) unstable; urgency=low

  * Fix build with GCC-6 (Closes: #831172)

 -- Afif Elghraoui <afif@debian.org>  Sun, 17 Jul 2016 00:00:34 -0700

consensuscore (1.0.2-1) unstable; urgency=medium

  * Add binary package for C++ headers and static library
    (Closes: #828837)
  * Rename source package and change section
  * Imported Upstream version 1.0.2 (git28dbb19 - 2016-05-06)
  * Bump Standards-Version to 3.9.8
  * Remove invalid architecture entries
  * Clean up d/rules and d/control
  * Update email address and copyright year
  * Make Python package descriptions consistent

 -- Afif Elghraoui <afif@debian.org>  Sun, 03 Jul 2016 03:12:49 -0700

python-pbconsensuscore (1.0.1-1) unstable; urgency=low

  * Run build-time tests
  * Limit package to x86 architectures
  * Build for all supported python versions
  * Imported Upstream version 1.0.1

 -- Afif Elghraoui <afif@ghraoui.name>  Tue, 24 Nov 2015 22:45:58 -0800

python-pbconsensuscore (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #794450)
  * Build Python3 package

 -- Afif Elghraoui <afif@ghraoui.name>  Thu, 03 Sep 2015 00:12:46 -0700
